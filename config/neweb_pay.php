<?php

return [
    /**
     * NewebPay's endpoint
     */
    'endpoint' => [
        'payment' => env('NEWEB_PAY_PAYMENT_ENDPOINT', 'https://ccore.newebpay.com/MPG/mpg_gateway'),
        'credit_card_cancel' => env('NEWEB_PAY_CANCEL_ENDPOINT', 'https://ccore.newebpay.com/API/CreditCard/Cancel'),
        'credit_card_close' => env('NEWEB_PAY_CLOSE_ENDPOINT', 'https://ccore.newebpay.com/API/CreditCard/Close'),
        'trade_info_query' => env('NEWEB_PAY_QUERY_ENDPOINT', 'https://ccore.newebpay.com/API/QueryTradeInfo'),
    ],

    /**
     * Merchant Id
     */
    'merchant_id' => env('MERCHANT_ID', 'MS35898676'),

    /**
     * API hash key
     */
    'hash_key' => env('HASH_KEY', 'KPWPpiFIDle4vYARQlZ1EEltDoAi6sjB'),

    /**
     * API hash Iv
     */
    'hash_iv' => env('HASH_IV', 'bshY3Fq9u8Cxz9Gn'),

    /**
     * Rule of parameter validation
     */
    'validation_rule' => [
        'payment' => [
            'itemDesc' => ['max:50'],
            'email' => ['email'],
            'optional.orderComment' => ['max:300']
        ],
        'cancel' => [
            'amount' => ['integer'],
            'indexType' => ['integer', 'in:1,2'],
            'optional.merchantOrderNo' => ['string'],
            'optional.tradeNo' => ['string'],
            'optional.notifyURL' => ['string', 'url'],
        ],
    ],
    
    /**
     * NewebPay's e-invoice settings
     */
    'electronic_invoice' => [
        /**
         * Invoice APIs endpoints
         */
        'endpoint' => [
            'open_invoice' => env('NEWEB_PAY_OPEN_INVOICE_ENDPOINT', 'https://cinv.ezpay.com.tw/Api/invoice_issue'),
            'search_invoice' => env('NEWEB_PAY_SEARCH_INVOICE_ENDPOINT', 'https://cinv.ezpay.com.tw/Api/invoice_search'),
        ],

        /**
         * Invoice Merchant Id
         */
        'merchant_id' => env('INVOICE_MERCHANT_ID', '31735454'),

        /**
         * Invoice API hash key
         */
        'hash_key' => env('INVOICE_HASH_KEY', '6uw7UaVYdikbo9ZJUfGDP0kb5fJRvtXp'),

        /**
         * Invoice API hash Iv
         */
        'hash_iv' => env('INVOICE_HASH_IV', 'ChG2W9rfalKEUNZP'),

        /**
         * Rule of parameter validation
         */
        'validation_rule' => [
            'open_invoice' => [
                'merchantOrderNo' => ['string', 'regex:/^\w+/'],
                'status' => ['string', 'in:1,0,3'],
                'category' => ['string', 'in:B2B,B2C'],
                'printFlag' => ['string', 'in:Y,N'],
                'taxType' => ['string', 'in:1,2,3,9'],
            ],
        ],
    ],
];
