<?php

namespace NewebPay\NewebPay;

use GuzzleHttp\Client;
use InvalidArgumentException;

abstract class ApiClient
{
    /**
     * The application instance.
     *
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected $app;

    /**
     * The http client, it's defined by endpoint
     *
     * @var null|\GuzzleHttp\Client
     */
    protected $client;

    /**
     * The configuration associated with this package
     *
     * @var string
     */
    protected $config;

    /**
     * The endpoint associated with the http client
     *
     * @var string
     */
    protected $endpoint;

    /**
     * The endpoint key which can get from associated config
     *
     * @var string
     */
    protected $endpointKey;

    /**
     * Create a new instance
     */
    public function __construct($app)
    {
        $this->app = $app;
        $this->endpoint = $this->getConfig($this->endpointKey);
        $this->client = $this->bootClientHandler();
    }

    /**
     * Get the configuration.
     *
     * @param string $name
     * @return string|array|null
     */
    protected function getConfig(string $name)
    {
        return $this->app['config']["neweb_pay.{$name}"];
    }

    /**
     * The method is used to boot the client handler.
     * It depends on the endpoint setting
     *
     * @throws \InvalidArgumentException
     *
     * @return \GuzzleHttp\Client
     */
    protected function bootClientHandler(): Client
    {
        if (empty($this->endpoint)) {
            throw new InvalidArgumentException(__CLASS__ . ' should declare $endpoint variable.');
        }

        return new Client(['base_uri' => $this->endpoint]);
    }

    /**
     * Get the response through http request
     *
     * @param string $method
     * @param string $uri
     * @param array $option
     *
     * @throws InvalidArgumentException
     *
     * @return string
     */
    protected function request(string $method, string $uri, array $option = []): string
    {
        $response =  $this->client->request($method, $uri, $option);
        if (200 !== $response->getStatusCode()) {
            throw new InvalidArgumentException('The http status of response is not 200 OK.');
        }

        return $response->getBody()->getContents();
    }
}
