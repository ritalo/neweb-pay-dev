<?php

namespace NewebPay\NewebPay;

use Carbon\Carbon;
use InvalidArgumentException;

class OpenInvoice extends ApiClient
{
    use ValidateTrait;
    use EncryptTrait;

    /**
     * {@inheritdoc}
     */
    protected $endpointKey = 'electronic_invoice.endpoint.open_invoice';

    /**
     * Create e-invoice
     *
     * @param string $merchantOrderNo 商店自訂訂單編號
     * @param string $status 開立發票方式 {1:即時開立發票, 0:等待觸發開立發票, 3:預約自動開立發票(須指定預計開立日期)}
     * @param string $category 發票種類 {B2B:買受人為營業人, B2C:買受人為個人}
     * @param string $buyerName 買受人名稱
     * @param string $buyerAddress 買受人地址
     * @param string $taxType 課稅別 {1:應稅, 2:零稅率, 3:免稅, 9:混合應稅與免稅或零稅率(限category為B2C時使用)}
     * @param float $taxRate 稅率
     * @param int $amount 銷售額(未稅)
     * @param int $taxAmt 稅額
     * @param int $totalAmt 發票金額(銷售額+稅額)
     * @param array $itemName 商品名稱
     * @param array $itemCount 商品數量
     * @param array $itemUnit 商品單位, 例：個、件、本、張
     * @param array $itemPrice 商品單價
     * @param array $itemAmt 商品小計(Category=B2B,此參數為未稅金額;Category=B2C,此參數為含稅金額)
     * @param string $printFlag 索取紙本發票 {Y:索取(營業人可於本平台列印此發票), N:不索取}
     * @param string $transNumber ezPay平台交易序號
     * @param int $createStatusTime 預計開立日期:當開立發票方式為預約自動開立發票時(status=3), 才需要帶此參數
     * @param string $buyerUBN 買受人統一編號
     * @param string $buyerEmail 買受人電子信箱 (當carrierType=2時, 則此參數為必填)
     * @param string $carrierType 載具類別, 當category=B2C時，才適用此參數 {0:手機條碼載具, 1:自然人憑證條碼載具, 2:ezPay電子發票載具}
     * @param string $carrierNumber 載具編號
     * @param string $loveCode 捐贈碼
     * @param string $customsClearance 報關標記,taxRate為零稅率時使用 {1:非經海關出口, 2:經海關出口}
     * @param int $amtSales 銷售額(課稅別應稅), taxType=9時需提供此欄位
     * @param int $amtZero 銷售額(課稅別零稅率), taxType=9時需提供此欄位
     * @param int $amtFree 銷售額(課稅別免稅), taxType=9時需提供此欄位
     * @param array $itemTaxType 商品課稅別, taxType=9時需提供此欄位, 課稅別參見 $taxType
     * @param string $comment 備註
     *
     * @throws \InvalidArgumentException
     *
     * @return array
     */
    public function get(
        string $merchantOrderNo,
        string $status,
        string $category,
        string $buyerName,
        string $taxType,
        float $taxRate,
        int $amount,
        int $taxAmt,
        int $totalAmt,
        array $itemName,
        array $itemCount,
        array $itemUnit,
        array $itemPrice,
        array $itemAmt,
        string $printFlag = 'Y',
        string $transNumber = '',
        int $createStatusTime = null,
        string $buyerUBN = '',
        string $buyerAddress = '',
        string $buyerEmail = '',
        string $carrierType = '',
        string $carrierNumber = '',
        string $loveCode = '',
        string $customsClearance = '',
        int $amtSales = null,
        int $amtZero = null,
        int $amtFree = null,
        array $itemTaxType = [],
        string $comment = ''
    ): array {
        $this->validateParams(
            [
                'merchantOrderNo' => $merchantOrderNo,
                'status' => $status,
                'category' => $category,
                'printFlag' => $printFlag,
                'taxType' => $taxType,
            ],
            'electronic_invoice.validation_rule.open_invoice'
        );

        $data = [
            'RespondType' => 'JSON',
            'Version' => '1.4',
            'TimeStamp' => (string) Carbon::now()->timestamp,
            'MerchantOrderNo' => $merchantOrderNo,
            'Status' => $status,
            'Category' => $category,
            'BuyerName' => $buyerName,
            'PrintFlag' => strtoupper($printFlag),
            'TaxType' => $taxType,
            'TaxRate' => $taxRate,
            'Amt' => $amount,
            'TaxAmt' => $taxAmt,
            'TotalAmt' => $totalAmt,
            'ItemName' => implode('|', $itemName),
            'ItemCount' => implode('|', $itemCount),
            'ItemUnit' => implode('|', $itemUnit),
            'ItemPrice' => implode('|', $itemPrice),
            'ItemAmt' => implode('|', $itemAmt),
            'TransNum' => $transNumber,
            'CreateStatusTime' => (null === $createStatusTime) ? '' : Carbon::createFromTimestamp($createStatusTime)->toDateString(),
            'BuyerUBN' => $buyerUBN,
            'BuyerAddress' => $buyerAddress,
            'BuyerEmail' => $buyerEmail,
            'CarrierType' => $carrierType,
            'CarrierNum' => rawurldecode(trim($carrierNumber)),
            'LoveCode' => $loveCode,
            'CustomsClearance' => $customsClearance,
            'AmtSales' => $amtSales ?? '',
            'AmtZero' => $amtZero ?? '',
            'AmtFree' => $amtFree ?? '',
            'ItemTaxType' => implode('|', $itemTaxType),
            'Comment' => $comment,
        ];
        $result = json_decode($this->request(
            'POST',
            $this->endpoint,
            [
                'form_params' => [
                    'MerchantID_' => $this->getConfig('electronic_invoice.merchant_id'),
                    'PostData_' => $this->encryptByAes(
                        $data,
                        $this->getConfig('electronic_invoice.hash_key'),
                        $this->getConfig('electronic_invoice.hash_iv')
                    ),
                ],
            ]
        ), true);

        if ('SUCCESS' === strtoupper(array_get($result, 'Status'))) {
            $content = json_decode(array_get($result, 'Result', ''), true);
            if (!$this->validateCheckCode(
                array_only($content, [
                    'MerchantID',
                    'InvoiceTransNo',
                    'MerchantOrderNo',
                    'TotalAmt',
                    'RandomNum',
                ]),
                array_get($content, 'CheckCode', ''),
                $this->getConfig('electronic_invoice.hash_key'),
                $this->getConfig('electronic_invoice.hash_iv')
            )) {
                throw new InvalidArgumentException('Check code 驗證失敗, 不合法的回傳值');
            }
        }

        return $result;
    }
}
