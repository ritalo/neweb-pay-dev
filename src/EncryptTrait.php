<?php

namespace NewebPay\NewebPay;

trait EncryptTrait
{
    /**
     * Encrypt data by AES
     *
     * @param array $data
     * @param string $hashKey
     * @param string $hashIv
     *
     * @return string
     */
    protected function encryptByAes(array $data, string $hashKey, string $hashIv): string
    {
        $httpQuery = '';
        if (!empty($data)) {
            $httpQuery = http_build_query($data);
        }
        return trim(bin2hex(openssl_encrypt(
            $this->addPadding($httpQuery),
            'AES-256-CBC',
            $hashKey,
            OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING,
            $hashIv
        )));
    }

    /**
     *
     * @param string $data
     *
     * @return string
     */
    protected function addPadding(string $data): string
    {
        $blockSize = 32;
        $pad = $blockSize - (\strlen($data) % $blockSize);
        $data .= str_repeat(\chr($pad), $pad);

        return $data;
    }
}
