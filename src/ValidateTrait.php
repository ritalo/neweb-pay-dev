<?php

namespace NewebPay\NewebPay;

use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

trait ValidateTrait
{
    /**
     * Validate the input parameters
     *
     * @param array $data
     * @param string $ruleType
     *
     * @throws InvalidArgumentException
     */
    protected function validateParams(array $data, string $ruleType)
    {
        $validator = Validator::make(array_dot($data), $this->getConfig($ruleType) ?? []);
        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->toJson());
        }
    }

    /**
     * Validate check code
     *
     * @param array $data
     * @param string $checkCode
     * @param string $hashKey
     * @param string $hashIv
     *
     * @return bool
     */
    protected function validateCheckCode(array $data, string $checkCode, string $hashKey, string $hashIv): bool
    {
        $httpQuery = '';
        if (!empty($data)) {
            ksort($data, SORT_STRING);
            $httpQuery = http_build_query($data);
        }

        return !(bool) strcmp(strtoupper(hash(
            'sha256',
            'HashIV=' . $hashIv . '&'. $httpQuery . '&HashKey=' . $hashKey
        )), $checkCode);
    }
}
