<?php

namespace NewebPay\NewebPay;

use Carbon\Carbon;

class CreditCardClose extends ApiClient
{
    use EncryptTrait;

    /**
     * {@inheritdoc}
     */
    protected $endpointKey = 'endpoint.credit_card_close';

    /**
     * Refund or apply credit card payment
     *
     * @param int $amount Order amount
     * @param string $merchantOrderNo Order number
     * @param string $tradeNo Order number from vendor
     * @param int $indexType Type of order number
     * @param int $closeType Close type for credit card {1:apply payment, 2:refund}
     * @param bool $cancel Cancel
     *
     * @return string
     */
    public function close(
        int $amount,
        string $merchantOrderNo,
        string $tradeNo,
        int $indexType,
        int $closeType,
        bool $cancel = false
    ): string {
        return $this->request(
            'POST',
            $this->endpoint,
            [
                'MerchantID_' => $this->getConfig('merchant_id'),
                'PostData_ ' => $this->encryptByAes(
                    [
                        'RespondType' => 'JSON',
                        'Version' => '1.0',
                        'Amt' => $amount,
                        'MerchantOrderNo' => $merchantOrderNo,
                        'TradeNo' => $tradeNo,
                        'IndexType' => $indexType,
                        'TimeStamp' => Carbon::now()->timestamp,
                        'CloseType' => $closeType,
                        'Cancel' => (int) $cancel,
                    ],
                    $this->getConfig('hash_key'),
                    $this->getConfig('hash_iv')
                ),
            ]
        );
    }
}
