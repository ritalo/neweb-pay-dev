<?php

namespace NewebPay\NewebPay;

use Carbon\Carbon;
use InvalidArgumentException;

class SearchInvoice extends ApiClient
{
    use ValidateTrait;
    use EncryptTrait;

    /**
     * {@inheritdoc}
     */
    protected $endpointKey = 'electronic_invoice.endpoint.search_invoice';

    /**
     * Search e-invoice details
     *
     * @param string $merchantOrderNo 訂單編號
     * @param string $totalAmt 發票金額
     * @param string $invoiceNumber 發票號碼
     * @param string $randomNum 發票防偽隨機碼
     * @param string $searchType 查詢方式 {0:使用發票號碼及隨機碼查詢, 1:使用訂單編號及發票金額查詢}
     *
     * @return array
     */
    public function getInvoice(
        string $merchantOrderNo,
        string $totalAmt,
        string $invoiceNumber,
        string $randomNum,
        string $searchType = '0'
    ): array {
        $data = [
            'RespondType' => 'JSON',
            'Version' => '1.1',
            'TimeStamp' => (string) Carbon::now()->timestamp,
            'SearchType' => $searchType,
            'MerchantOrderNo' => $merchantOrderNo,
            'TotalAmt' => $totalAmt,
            'InvoiceNumber' => $invoiceNumber,
            'RandomNum' => $randomNum,
        ];
        $response = json_decode($this->request(
            'POST',
            $this->endpoint,
            [
                'form_params' => [
                    'MerchantID_' => $this->getConfig('electronic_invoice.merchant_id'),
                    'PostData_' => $this->encryptByAes(
                        $data,
                        $this->getConfig('electronic_invoice.hash_key'),
                        $this->getConfig('electronic_invoice.hash_iv')
                    ),
                ],
            ]
        ), true);

        $result = [];
        if ('SUCCESS' === strtoupper(array_get($response, 'Status'))) {
            $result = json_decode(array_get($response, 'Result', ''), true);
            if (!$this->validateCheckCode(
                array_only($result, [
                    'MerchantID',
                    'InvoiceTransNo',
                    'MerchantOrderNo',
                    'TotalAmt',
                    'RandomNum',
                ]),
                array_get($result, 'CheckCode', ''),
                $this->getConfig('electronic_invoice.hash_key'),
                $this->getConfig('electronic_invoice.hash_iv')
            )) {
                throw new InvalidArgumentException('Check code 驗證失敗, 不合法的回傳值');
            }
        }
        
        return $result;
    }

    /**
     * Get e-invoice api parameters
     *
     * @param string $merchantOrderNo 訂單編號
     * @param string $totalAmt 發票金額
     * @param string $invoiceNumber 發票號碼
     * @param string $randomNum 發票防偽隨機碼
     * @param string $searchType 查詢方式 {0:使用發票號碼及隨機碼查詢, 1:使用訂單編號及發票金額查詢}
     *
     * @return array
     */
    public function getPostData(
        string $merchantOrderNo,
        string $totalAmt,
        string $invoiceNumber,
        string $randomNum,
        string $searchType = '0'
    ): array {
        $data = [
            'RespondType' => 'JSON',
            'Version' => '1.1',
            'TimeStamp' => (string) Carbon::now()->timestamp,
            'SearchType' => $searchType,
            'MerchantOrderNo' => $merchantOrderNo,
            'TotalAmt' => $totalAmt,
            'InvoiceNumber' => $invoiceNumber,
            'RandomNum' => $randomNum,
            'DisplayFlag' => '1',// 是否於第三方平台網頁顯示發票查詢結果 {1:，將網頁控制權送至第三方平台網頁, 顯示發票查詢結果}
        ];

        return [
            'merchantId' => $this->getConfig('electronic_invoice.merchant_id'),
            'postData' => $this->encryptByAes(
                $data,
                $this->getConfig('electronic_invoice.hash_key'),
                $this->getConfig('electronic_invoice.hash_iv')
            ),
        ];
    }

}
