<?php

namespace NewebPay\NewebPay;

use Carbon\Carbon;

class CreditCardCancel extends ApiClient
{
    use ValidateTrait;
    use EncryptTrait;

    /**
     * {@inheritdoc}
     */
    protected $endpointKey = 'endpoint.credit_card_cancel';

    /**
     * Cancel for credit card
     *
     * @param int $amount Order amount
     * @param int $indexType Type of order number
     * @param array $optional
     *
     * @return string
     */
    public function cancel(int $amount, int $indexType, array $optional = []): string
    {
        $this->validateParams(
            ['amount' => $amount, 'indexType' => $indexType, 'optional' => $optional],
            'validation_rule.cancel'
        );
        return $this->request(
            'POST',
            $this->endpoint,
            [
                'MerchantID_' => $this->getConfig('merchant_id'),
                'PostData_ ' => $this->encryptByAes(
                    [
                        'RespondType' => 'JSON',
                        'Version' => '1.0',
                        'Amt' => $amount,
                        'IndexType' => $indexType,
                        'TimeStamp' => Carbon::now()->timestamp,
                        /**
                         * Order number
                         */
                        'MerchantOrderNo' => array_get($optional, 'merchantOrderNo'),
                        /**
                         * Order number of vendor
                         */
                        'TradeNo' => array_get($optional, 'tradeNo'),
                        /**
                         * URL to notify payment result
                         */
                        'NotifyURL' => array_get($optional, 'notifyURL'),
                    ],
                    $this->getConfig('hash_key'),
                    $this->getConfig('hash_iv')
                ),
            ]
        );
    }
}
