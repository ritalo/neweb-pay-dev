<?php

namespace NewebPay\NewebPay;

use Carbon\Carbon;

class TradeInfo extends ApiClient
{
    /**
     * {@inheritdoc}
     */
    protected $endpointKey = 'endpoint.trade_info_query';

    /**
     * Query order
     *
     * @param string $merchantOrderNo
     * @param int $amount
     *
     * @return string
     */
    public function query(string $merchantOrderNo, int $amount): array
    {
        $data = [
            'MerchantID' => $this->getConfig('merchant_id'),
            'Version' => '1.1',
            'RespondType' => 'JSON',
            'TimeStamp' => Carbon::now()->timestamp,
            'MerchantOrderNo' => $merchantOrderNo,
            'Amt' => $amount,
        ];

        return json_decode($this->request(
            'POST',
            $this->endpoint,
            [
                'form_params' => array_merge($data, [
                    'CheckValue' => $this->createCheckCode(array_only($data, [
                        'Amt',
                        'MerchantID',
                        'MerchantOrderNo',
                    ]))
                ])
            ]
        ), true);
    }

    /**
     * Create check code
     *
     * @param array $data
     *
     * @return string
     */
    protected function createCheckCode(array $data): string
    {
        $httpQuery = '';
        if (!empty($data)) {
            ksort($data, SORT_STRING);
            $httpQuery = http_build_query($data);
        }
        return strtoupper(hash(
            'sha256',
            'IV=' . $this->getConfig('hash_iv') . '&'. $httpQuery . '&Key=' . $this->getConfig('hash_key')
        ));
    }
}
