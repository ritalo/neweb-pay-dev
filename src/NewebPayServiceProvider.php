<?php

namespace NewebPay\NewebPay;

use Illuminate\Support\ServiceProvider;

class NewebPayServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $source = \dirname(__DIR__).'/config/neweb_pay.php';
        $this->publishes([$source => config_path('neweb_pay.php')]);
        $this->mergeConfigFrom($source, 'neweb_pay');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Payment::class, function () {
            return new Payment($this->app);
        });
        $this->app->singleton(CreditCardCancel::class, function () {
            return new CreditCardCancel($this->app);
        });
        $this->app->singleton(CreditCardClose::class, function () {
            return new CreditCardClose($this->app);
        });
        $this->app->singleton(TradeInfo::class, function () {
            return new TradeInfo($this->app);
        });
        $this->app->singleton(OpenInvoice::class, function () {
            return new OpenInvoice($this->app);
        });
        $this->app->singleton(SearchInvoice::class, function () {
            return new SearchInvoice($this->app);
        });
    }
}
