<?php

namespace NewebPay\NewebPay;

use Carbon\Carbon;

class Payment extends ApiClient
{
    use ValidateTrait;
    use EncryptTrait;

    /**
     * {@inheritdoc}
     */
    protected $endpointKey = 'endpoint.payment';

    /**
     * Create order for payment
     *
     * @param string $merchantOrderNo Order number
     * @param int $amount Order amount (NTD)
     * @param string $itemDesc Product name
     * @param bool $loginType Whether to login NewebPay member
     * @param string $email User email for notification
     * @param array $optional Optional settings
     *
     * @return array
     */
    public function createOrder(
        string $merchantOrderNo,
        int $amount,
        string $itemDesc,
        bool $loginType,
        string $email,
        array $optional = []
    ): array {
        $this->validateParams([
            'itemDesc' => $itemDesc,
            'email' => $email,
            'optional' => $optional,
        ], 'validation_rule.payment');
        $info = $this->encryptTradeInfoByAes([
            'MerchantID' => $this->getConfig('merchant_id'),
            'RespondType' => 'JSON',
            'TimeStamp' => Carbon::now()->timestamp,
            'Version' => '1.5',
            'MerchantOrderNo' => $merchantOrderNo,
            'Amt' => $amount,
            'ItemDesc' => $itemDesc,
            'LoginType' => (int) $loginType,
            'Email' => $email,
            /**
             * Whether to let the user modify email (default:true)
             */
            'EmailModify' => array_get($optional, 'emailModify', 1),
            /**
             * Language (default:Traditional Chinese)
             */
            'LangType' => array_get($optional, 'langType', ''),
            /**
             * Limit seconds of the transaction (default:disable)
             */
            'TradeLimit' => array_get($optional, 'tradeLimit') ?? 0,
            /**
             * Store URL to return, after the payment is completed
             */
            'ReturnURL' => array_get($optional, 'returnUrl'),
            /**
             * URL to notify payment result
             */
            'NotifyURL' => array_get($optional, 'notifyUrl'),
            /**
             * Return URL when user cancel this payment
             */
            'ClientBackURL' => array_get($optional, 'clientBackUrl'),
            /**
             * Comment to this payment
             */
            'OrderComment' => array_get($optional, 'orderComment'),
            /**
             * Enable credit card one-time payment (default:false)
             */
            'CREDIT' => array_get($optional, 'credit') ?? 0,
            /**
             * Enable GooglePay payment (default:false)
             */
            'ANDROIDPAY' => array_get($optional, 'androidPay') ?? 0,
            /**
             * Enable SamsungPay payment (default:false)
             */
            'SAMSUNGPAY' => array_get($optional, 'samsungPay') ?? 0,
            /**
             * Installment for credit card
             */
            'InstFlag' => implode(',', array_get($optional, 'instFlag', [])),
            /**
             * Enable reward points for credit card
             */
            'CreditRed' => array_get($optional, 'creditRed') ?? 0,
            /**
             * Enable GooglePay payment (default:false)
             */
            'UNIONPAY' => array_get($optional, 'UnionPay') ?? 0,
            /**
             * Enable Web ATM
             */
            'WEBATM' => array_get($optional, 'webAtm') ?? 0,
            /**
             * Enable Enable ATM transfer
             */
            'VACC' => array_get($optional, 'vacc') ?? 0,
            /**
             * Enable convenience store code payment
             */
            'CVS' => array_get($optional, 'cvs') ?? 0,
            /**
             * Enable convenience store barcode payment
             */
            'BARCODE' => array_get($optional, 'barCode') ?? 0,
            /**
             * Enable ezPay electronic wallet payment
             */
            'P2G' => array_get($optional, 'p2g') ?? 0,
            /**
             * Enable convenience store logistics
             */
            'CVSCOM' => array_get($optional, 'cvsCom') ?? 0,
            /**
             * URL of pick-up number
             * - suitable for VACC, CVS, BARCODE, CVSCOM payment
             */
            'CustomerURL' => array_get($optional, 'customerUrl'),
            /**
             * Payment expiration date (default:7 days)
             * - suitable for VACC, CVS, BARCODE, CVSCOM payment
             */
            'ExpireDate' => array_get($optional, 'expireDate'),
        ]);
        /**
         * Remember customer's payment data
         * TokenTerm: the identifier corresponding to customer, ex: id, email
         * TokenTermDemand: specify the credit card information that the payer needs to fill out
         */
        if (array_key_exists('tokenTerm', $optional)) {
            $info = array_merge([
                'TokenTerm' => $optional['tokenTerm'],
                'TokenTermDemand' => array_get($optional, 'tokenTermDemand', 1),
            ], $info);
        }

        return [
            'merchantID' => $this->getConfig('merchant_id'),
            'tradeInfo' => $info,
            'tradeSha' => $this->encryptTradeBySha256($info),
            'version' => '1.5',
        ];
    }

    /**
     * Encrypt trade information by AES
     *
     * @param array $data
     *
     * @return string
     */
    protected function encryptTradeInfoByAes(array $data): string
    {
        $httpQuery = '';
        if (!empty($data)) {
            $httpQuery = http_build_query($data);
        }
        return trim(bin2hex(openssl_encrypt(
            $this->addPadding($httpQuery),
            'aes-256-cbc',
            $this->getConfig('hash_key'),
            OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING,
            $this->getConfig('hash_iv')
        )));
    }

    /**
     * Decrypt trade information by AES
     *
     * @param string $data
     *
     * @return array
     */
    public function decryptTradeInfoByAes(string $data): array
    {
        $decrypt =  explode('&', $this->stripPadding(openssl_decrypt(
            hex2bin($data),
            'aes-256-cbc',
            $this->getConfig('hash_key'),
            OPENSSL_RAW_DATA|OPENSSL_ZERO_PADDING,
            $this->getConfig('hash_iv')
        )));

        return json_decode($decrypt[0], true);
    }

    /**
     * Strip padding
     *
     * @param string $data
     *
     * @return bool|string
     */
    protected function stripPadding(string $data)
    {
        $last = \ord(substr($data, -1));
        $lastChr = \chr($last);
        if (preg_match("/$lastChr{" . $last . '}/', $data)) {
            return substr($data, 0, \strlen($data) - $last);
        }
        return false;
    }

    /**
     * Encrypt trade information by SHA-256
     *
     * @param string $data
     *
     * @return string
     */
    protected function encryptTradeBySha256(string $data): string
    {
        return strtoupper(hash(
            'sha256',
            'HashKey=' . $this->getConfig('hash_key') . '&'. $data . '&HashIV=' . $this->getConfig('hash_iv')
        ));
    }
}
